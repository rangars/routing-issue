var map = L.map('map', {
	minZoom: 13,
	maxZoom: 18,
	zoomDelta: 0.25,
	zoomSnap: 0.2
});

L.tileLayer('https://api.mapbox.com/styles/v1/rengars/ckojqr0pr018u18ozl9tww43p/tiles/512/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoicmVuZ2FycyIsImEiOiJja2k1enJ6dmszcTZxMnJwNTFnOGZhbGVtIn0.L8YRslW4rVaCFfqFKAvLvA', {
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1,
		attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
	}).addTo(map);

	var LeafIcon = L.Icon.extend({
		options: {
			//shadowUrl: 'leaf-shadow.png',
			iconSize:     [15, 15],
			shadowSize:   [15, 15],
			iconAnchor:   [15, 15],
			shadowAnchor: [4, 62],
			popupAnchor:  [-3, -76]
		}
	});
	const green = 'http://lifeonline.co/map/demo/images/pointer/green.png';
	const blue = 'http://lifeonline.co/map/demo/images/pointer/blue.png';
	const yellow = 'http://lifeonline.co/map/demo/images/pointer/yellow.png';
	const red = 'http://lifeonline.co/map/demo/images/pointer/red.png';
	const seagreen = 'http://lifeonline.co/map/demo/images/pointer/dog-green.png';
	const purple = 'http://lifeonline.co/map/demo/images/pointer/purple.png';
	const grey = 'http://lifeonline.co/map/demo/images/pointer/grey.png';
	const black = 'http://lifeonline.co/map/demo/images/pointer/black.png';
	const darkblue = 'http://lifeonline.co/map/demo/images/pointer/dark-blue.png';
	const ferry = 'http://lifeonline.co/map/demo/images/pointer/ferry.png';
	
	const lineColorGreen = "#00b050";
	const lineColorDogGreen = "#92d050";
	const lineColorRed = "red";
	const lineColorBlue = "#4472c4";
	const lineColorDBlue = "#002060";
	const lineColorPurple = "purple";
	const lineColorYellow = "#ffff00";
	const lineColorBlack = "black";
	const lineColorGrey = "#878787";
	const lineColorAqua = "aqua";
	const lineColorOrange = "orange";
	const lineColorcook = "#0000";


	let categorys = {

		"Dandy1":[
		{
			"coordiates":[47.35661778898828, 8.53612361950723],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.35644339334677, 8.535741319581707],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all2",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.35923382950216, 8.53603993197039],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all3",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.35921728292755, 8.536052149772795],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all4",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.3623596623052, 8.535636526485991],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all5",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.36573880850843, 8.539777880132],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all6",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.36671982654783, 8.540614697629497],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all7",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.369742690165815, 8.539359406032897],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all8",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.37240206473725, 8.538308005327279],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all9",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.37574853921132, 8.539383579548688],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all10",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{

			"coordiates":[47.36671982654783, 8.540614697629497],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.369742690165815, 8.539359406032897],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37240206473725, 8.538308005327279],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"2"
			
		},
		{

			"coordiates":[47.37574853921132, 8.53938357954868],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"2"
			
		},
		{

			"coordiates":[47.374142857456455, 8.538713024823526],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"2"
			
		},
		{

			"coordiates":[47.372215703319625, 8.540756339934044],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"2"
			
		},
		{

			"coordiates":[47.37170345264268, 8.54093335909596],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"2"
			
		},
		{

			"coordiates":[47.371071648710775, 8.5412685307699],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"Landesmuseum",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"2"
			
		},
	
				],
		"parade":[
		{

			"coordiates":[47.379193793447314, 8.540527747801566],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Landesmuseum",
			"popup":"<h1>Landesmuseum</h1><h2>Museumstrasse, 2, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: See the Einfach Zürich (Simply Zurich) section of the museum</p>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37132120668228, 8.540958168529537],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"St Peter",
			"popup":"<h1>St Peter</h1><h2>St Peterhofstatt, 1, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: See the tower clockface. Said to be the largest in Europe. Zurich traditionally measures time by it</p>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37075787973946, 8.541748705384062],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Wühre",
			"popup":"<h1>Wühre</h1><h2>Wühre, 7, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Savour the views across the Limmat river</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36986431277808, 8.541599041391986],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Fraumünster",
			"popup":"<h1>Fraumünster</h1><h2>Münsterhof, 2, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Go inside for the Chagall windows</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37158504022299, 8.54311998148217],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Haue",
			"popup":"<h1>Zunfthaus zur Haue</h1><h2>Limmatquai, 52, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Savour the views across the Limmat river</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
{

			"coordiates":[47.37108304093755, 8.54324242459495],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Rüden",
			"popup":"<h1>Haus zum Rüden</h1><h2>Limmatquai, 42, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: See the wooden barrel-vaulted ceiling in the Gotischer Saal with a fine view over the Limmat. Place the hightime Cook</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37024564854137, 8.544075464000239],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Grossmünster",
			"popup":"<h1>Grossmünster</h1><h2>Grossmünsterplatz, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Go inside for the windows by Sigmar Polke and go round the back to visit the restored cloister.</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37051277272987, 8.548223984934737],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Kunsthaus",
			"popup":"<h1>Kunsthaus</h1><h2>Heimplatz, 1, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Visit the museum, whose big new extension designed by David Chipperfield opens later this year. Fly the hightime tower outside the museum.</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37312057233375, 8.547742106810125],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Rechberg",
			"popup":"<h1>Haus zum Rechberg</h1><h2>Hirschengraben, 40, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: The house is the seat of Zurich's cantonal government. Visit the garden at the back. At the top of the garden, see Undine by notorious Zurich graffiti artist Harald Naegeli. Vew the hightime Trader next to it. (On one day in September you can take a tour of the house itself with openhouse-zuerich.org. The house has recently been renovated by Tilla Theus and features a painting of a girlfriend by Henry Fuseli.)</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36527739879318, 8.54630220863508],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Opera",
			"popup":"<h1>Opera</h1><h2>Sechseläutenplatz, 1, 8008</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Sit on the Sechseläutenplatz in front of the Opera House and watch the comings and goings. View the Glockenspiel Parade against the trees on the Stadelhoferplatz nearby</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37089981248775, 8.539448191801377],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Leuenhof",
			"popup":"<h1>Leuenhof (building embellished with lions)</h1><h2>Bahnhofstrasse, 32, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Find as many lions on the buildng as you can</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},{

			"coordiates":[47.37167367811796, 8.542612533404075],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Rathaus",
			"popup":"<h1>Rathaus (building decorated with lions)</h1><h2>Limmatquai, 55, 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: See the golden lions next to the door. Enjoy the many busts of historical and legendary figures over the ground floor windows</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},{

			"coordiates":[47.35997281333482, 8.537112489144498],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Löwendenkmal",
			"popup":"<h1>Löwendenkmal (Lion statue)</h1><h2>Mythenquai, Hafen Enge, 8002</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/><p>Fun Tips: Get a good view of the mountains next to the Lion statue in the harbourTry Kellers, new at the Hafen Beiz Enge Kiosk/restaurant.</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},

		],
		"Dandy":[
		{

			"coordiates":[47.36787948662207, 8.539937992490522],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy1",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25 , 8001</h2><img src='images/cha/danndy/GLOBUS L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36993223985974, 8.539272803140268],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy2",
			"popup":"<h1>2FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.3723736346859, 8.538317940843143],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy3",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37457152531971, 8.538650529499407],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy4",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37690009824256, 8.539857524530218],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy5",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.370555378061894, 8.538309852261136],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Fabric Frontline",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr, 25, 8001</h2><img style='width:100%;' src='images/cha/danndy/FABRIC-FRONTLINE-L.JPEG'/>Fun Tips: Ogle the silk foulards</p>",
			"is_waypoints":"0",
            "is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37603686467669, 8.537734075603666],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Globus",
			"popup":"<h1>Globus</h1><h2>Schweizergasse, 11, 8001</h2><img style='width:100%;' src='images/cha/danndy/GLOBUS-L.JPEG'/><p>Fun Tips: Go for the food</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37084822083156, 8.539447470184363],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Grieder",
			"popup":"<h1>Grieder</h1><h2>Bahnhofstr, 30, 8001</h2><img style='width:100%;' src='images/cha/danndy/GRIEDER-L.JPEG'/><p>Fun Tips: Try the rooftop bar</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37443216539481, 8.537649978818088],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Jelmoli",
			"popup":"<h1>Jelmoli</h1><h2>Seidengasse, 1, 8001</h2><img style='width:100%;' src='images/cha/danndy/Jelmoli-L.JPEG'/><p>Fun Tips: Seek out the Swiss brands. Underwear: Blue Lemon, Calida, Fogal, Hanro, Triumph, Zimmerli. Shoes: Bally, Benci Brothers, On. Skincare: La Prairie, Valmont,Cellcosmet </p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3751445679892, 8.539237879293651],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Kurz",
			"popup":"<h1>Kurz</h1><h2>Bahnhofstr, 80, 8001</h2><img style='width:100%;' src='images/cha/danndy/KURZ-L.JPEG'/><p>Fun Tips: Go for the Glockenspiel in front of the store that plays at 11.00 and 14.00. Play the hightime Glockenspiel next to it.</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.374903829121564, 8.538954708028399],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Modissa",
			"popup":"<h1>Modissa</h1><h2>Bahnhofstr, 74, 8001</h2><img style='width:100%;' src='images/cha/danndy/Modissa-L.JPEG'/><p>Fun Tips: Rise to the rooftop restaurant (separately run from the store)</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37576306043012, 8.53972690772336],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"PKZ Women",
			"popup":"<h1>PKZ Women</h1><h2>Bahnhofstr,88, 8001</h2><img style='width:100%;' src='images/cha/danndy/PKZ-L .JPEG'/><p>Fun Tips: Watch the Julian Opie video installation in front of the store</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		}
		],
		"Raver":[
			{

			"coordiates":[47.36191881440891, 8.547143894365517],
			"icon":purple,
			"lineColor" : lineColorPurple,
			"pointernae":"Raver1",
			"popup":"<h1>Aphrodite statue</h1><h2>Street : Near General Guisan Quai </h2> <h2>Street no.: 37, Postal code: 8002</h2>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36654790673802, 8.544515326243971],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver2",
			"popup":"<h1>2FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3670945585057, 8.543939474684397],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver20",
			"popup":"<h1>2FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36660042565368, 8.54172933449162],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver21",
			"popup":"<h1>2FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36573880850843, 8.539777880132],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver3",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36444944045175, 8.536965731311852],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver31",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3623596623052, 8.535636526485991],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver4",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.35921728292755, 8.536052149772795],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver5",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.364160, 8.536977],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Aphrodite",
			"popup":"<h1>Aphrodite statue</h1><h2>Near General Guisan Quai, 32, 8002</h2><img style='width:100%;' src='images/cha/raver/aphrodite-L.JPEG'/><p>Fun Tips: Walk a few metres from here to the lake for a classic view of the Alps (also from the Ganymede statue)</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3661625935621, 8.54122766369607],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Ganymede",
			"popup":"<h1>Ganymede statue</h1><h2>Bürkliplatz, 8001</h2><img style='width:100%;' src='images/cha/raver/ganymede-L.JPEG'/><p>Fun Tips: Enjoy the classic view of the alps, then take a short cruise on the lake, starting from the Bürkliplatz pier nearby, to see bathers, rowers, sailors and more on the Zürisee</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.365241997084965, 8.54553183570942],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"David",
			"popup":"<h1>David statue</h1><h2>Sechseläutenplatz, 8001</h2><img style='width:100%;' src='images/cha/raver/david-P1.JPEG'/><p>Fun Tips: Feeling energetic and want to get out on the lake, walk south and take a pedal boat or a standup paddle board from one of the nearby boat rental shops on the lakeshore  </p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		}

		],
		"Trader":[
			{
	
				"coordiates":[47.35923382950216, 8.53603993197039],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader1",
				"popup":"<h1>UBS</h1><h2>Street : Bahnhofstrasse. </h2> <h2>Street no.: 45, Postal code: 8001</h2>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.35921728292755, 8.536052149772795],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader2",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.3623596623052, 8.535636526485991],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader3",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36443277533463, 8.536977117066218],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader4",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36573880850843, 8.539777880132],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader5",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36671982654783, 8.540614697629497],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader6",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.369742690165815, 8.539359406032897],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader7",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37240206473725, 8.538308005327279],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader8",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37206080909378, 8.538207393210707],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"UBS",
				"popup":"<h1>UBS</h1><h2>Bahnhofstrasse,45, 8001</h2><img style='width:100%;' src='images/cha/trader/UBS-L.JPEG'/><p>Fun Tips: Look for the John Armleder painting in the recently renovated banking hall</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37011591477958, 8.538622849609295],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"CS",
				"popup":"<h1>CS</h1><h2>Paradeplatz,8, 8001</h2><img style='width:100%;' src='images/cha/trader/CREDIT-SUISSE-L.JPEG'/><p>Fun Tips: Go into the internal courtyard of the Credit Suisse building (entry from Paradeplatz is right in middle of Credit Suisse buiding)and view the hightime Trader. Then cross the Paradeplatz, have a coffee in Sprüngli opposite and watch the comings and goings</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36832572054428, 8.539397607417126],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"ZKB",
				"popup":"<h1>Zurich Cantonal Bank</h1><h2>Bahnhofstrasse,9, 8001</h2><img style='width:100%;' src='images/cha/trader/ZKB-L.JPEG'/><p>Fun Tips: Use the free 'Büro Züri' coworking space in the Zurich Cantonal Bank headquarters. The hightime Trader is probably using it. Maybe the Raver is using it too?</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36784023285354, 8.540630934647584],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"SNB",
				"popup":"<h1>Swiss National Bank</h1><h2>Börsenstrasse,15, 8001</h2><img style='width:100%;' src='images/cha/trader/SNB-L.JPEG'/><p>Fun Tips: View the interactive media wall and visit the walk-in gallery to learn the history of the Swiss National Bank </p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.359994517901605, 8.534820663192868],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Swiss Re",
				"popup":"<h1>Swiss Re</h1><h2>Mythenquai,50/60, 8002</h2><img style='width:100%;' src='images/cha/trader/Swiss-Re-L1.JPEG'/><p>Fun Tips: Explore reflections of the lake in the waved glass front of the 2017 Swiss Re Next building by architects Diener and Diener </p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36229027358674, 8.535049164922365],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Zurich",
				"popup":"<h1>Zurich</h1><h2>Mythenquai,2, 8002</h2><img style='width:100%;' src='images/cha/trader/Zurich-L.JPEG'/><p>Fun Tips: See the Springbrunnen in the lake. Sponsored by Zurich insurance and designed by Andres Bosshard, this fountain has water jets whose rhythm is aligned with the earth's vibrations.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36298521245633, 8.53469255828037],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Swiss Life",
				"popup":"<h1>Swiss Life</h1><h2>General Guisan Quai,40, 8022</h2><img style='width:100%;' src='images/cha/trader/Swiss-Life-L.JPEG'/><p>Fun Tips: Walk over to the Volière. Swiss Life looks after people's lives. The Volière, close by Swiss Life on the lake side of the Mythenquai, looks after birds' lives. It is a bird hospital. The highlife Dog may be there having a good look at his feathered friends</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			}
			
			],
			"Cook":[
			{
	
				"coordiates":[47.36772605584657, 8.545791464001539],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook1",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.37248753928834, 8.544584034583314],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook2",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.372862535679026, 8.545791952571623],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook3",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.37261397433702, 8.5405888490071],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook4",
				"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},
			 {
	
				"coordiates":[47.36772605584657, 8.545791464001539],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Kronenhalle",
				"popup":"<h1>Kronenhalle</h1><h2>Rämistr,4, 8001</h2><img style='width:100%;' src='images/cha/cook/Kronenhalle-L.JPEG'/><p>Fun Tips: See the paintings in the restaurant. Get there early and have a drink in the bar.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37248753928834, 8.544584034583314],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Oepfelchammer",
				"popup":"<h1>Oepfelchammer</h1><h2>Rindermarkt,12, 8001</h2><img style='width:100%;' src='images/cha/cook/Oepfelchammer-L.JPEG'/><p>Fun Tips: Eat in the Weinstube Oeli upstairs. After a few glasses of wine, try jumping between one of the wooden beams and the ceiling and exiting on the other side of the beam. It is a traditional but extremely difficult trick to pull off.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.372862535679026, 8.545791952571623],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Neumarkt",
				"popup":"<h1>Neumarkt</h1><h2>Neumarkt,5, 8001</h2><img style='width:100%;' src='images/cha/cook/Neumarkt-L.JPEG'/><p>Fun Tips: In high summer, try the cool hidden garden at the back.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37261397433702, 8.5405888490071],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Hotel Kindli",
				"popup":"<h1>Kindli</h1><h2>Pfalzgasse,1, 8001</h2><img style='width:100%;' src='images/cha/cook/KINDLI-L.JPEG'/><p>Fun Tips: Buy a dancing hare at En Soie next door. Exit the shop, turn right, go across the road and, opposite the fountain, look up to see two En Soie hares dancing on the roof.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37249303672568, 8.548765293294448],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Hotel Florhof",
				"popup":"<h1>Florhof</h1><h2>Florhofgasse,4, 8001</h2><img style='width:100%;' src='images/cha/cook/Florhof-L.JPEG'/><p>Fun Tips: Eat outside in the pretty courtyard.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37182070851623, 8.536362507420666],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Kaufleuten",
				"popup":"<h1>Kaufleuten</h1><h2>Pelikanplatz,18, 8001</h2><img style='width:100%;' src='images/cha/cook/Kaufleuten-L.JPEG'/><p>Fun Tips: Restaurant, Club, and Cultural Program, go for the food, stay for the fun.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37115154428343, 8.541279264921258],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Veltlinerkeller",
				"popup":"<h1>Veltlinerkeller</h1><h2>Schlüsselgasse,8, 8001</h2><img style='width:100%;' src='images/cha/cook/Veltliner-Keller-L.JPEG'/><p>Fun Tips: Ask to view the wine cellar</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.373741034236815, 8.536332192395793],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Hiltl",
				"popup":"<h1>Hiltl </h1><h2>St Anna Gasse,18, 8001</h2><img style='width:100%;' src='images/cha/cook/HILTL-L.JPEG'/><p>Fun Tips: Become a vegetarian…the food is good and sells by the kilo.</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			}	
	
			],
			"Dog":[
		{
			"coordiates":[47.38222154139295, 8.571709249405291],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train17",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.379170392755455, 8.570422191631994],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train16",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37873213553508, 8.569703763080057],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train15",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37841220118346, 8.566927187383664],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train14",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.379407041639354, 8.559943671491382],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train13",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.378850473037495, 8.559102319771698],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train12",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.37700532977674, 8.559730137789522],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train11",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.3755283151962, 8.55855861394079],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train10",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},	
		{
			"coordiates":[47.37537492685848, 8.557879037447242],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train9",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},	
		{
			"coordiates":[47.376312863762074, 8.55580796070538],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train8",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37680372617565, 8.554843639165764],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train7",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.37710616472855, 8.55267541955001],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train6",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37599729702353, 8.553393830764845],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train5",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37478761261234, 8.552015243236694],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train4",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.37462982360718, 8.549633504682333],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train2",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},	
			{
			"coordiates":[47.37207886870736, 8.550636676848717],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train1",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
			{

			"coordiates":[47.36835739124954, 8.547122299732871],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train0",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{

			"coordiates":[47.36724873228388, 8.54503766189074],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog1",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> <h2>Street no.: , Postal code: 8001</h2>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36650192442704, 8.544351761539165],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog2",
			"popup":"<h1>2FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3614910815177, 8.547392939891346],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog3",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.35807200808734, 8.547736208994438],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog4",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.35310844649945, 8.552288286520369],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog5",
			"popup":"<h1>3FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36724873228388, 8.54503766189074],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Bellevue",
			"popup":"<h1>Bellevue(Trams)</h1><h2>Bellevueplatz, 8001</h2><img style='width:100%;' src='images/cha/dog/Bellevue-L.JPEG'/><p>Fun Tips: Have a sausage, mustard, and a bread roll in the Vorderer Sternen. In the middle of the trams, have a coffee at the bar in the Caffè & Bar Bellevue and watch the tram ballet.</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.353168391784806, 8.552314785054762],
			"icon":seagreen,
            "lineColor" : lineColorDogGreen,
			"pointernae":"Zürichhorn",
			"popup":"<h1>Zürichhorn</h1><h2>nr Zürichhorn, 8008</h2><img style='width:100%;' src='images/cha/dog/Zurichhorn -Eureka-L.JPEG'/><p>Fun Tips: Enjoy the mobile sculpture Heureka by Jean Tinguely and the view of the mountains</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.381646245724426, 8.571352649009718],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Zoo",
			"popup":"<h1>Zoo</h1><h2>Zürichbergstrasse, 8044</h2><img style='width:100%;' src='images/cha/dog/Zoo-Zurichberg-L.JPEG'/><p>Fun Tips: See the excellent Zoo (no dogs allowed except hightime Dog). See James Joyce grave in nearby Fluntern cemetery.</p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		}

		],
		
	  "Guildsman":[
		{
	
			"coordiates":[47.36671982654783, 8.540614697629497],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman1",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.369742690165815, 8.539359406032897],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman2",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37240206473725, 8.538308005327279],
			"icon": purple,
			"lineColor" : lineColorPurple,
			"pointernae":"Guildsman3",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"0",
			"is_all_waypoints":"1"
			
		},
		{

			"coordiates":[47.374498887967306, 8.538794171917969],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman4",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37437897775108, 8.543032084097394],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman5",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36910482485842, 8.543443471303119],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman6",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.366899129232046, 8.5458334642332],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman7",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.365546545956015, 8.546411220199175],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman8",
			"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.370831213827024, 8.54345544870451],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Zimmerleuten",
			"popup":"<h1>Zunfthaus zur Zimmerleuten (Builders, Carpenters, Coopers Guild)</h1><h2>Limmatquai,40, 8001</h2><img style='width:100%;' src='images/cha/guildsman/Zimmerleuten-L.JPEG'/><p>Fun Tips: Wrap yourself in rugs and eat an Open Air Fondue in all weathers</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37103944706058, 8.543075948405097],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Rüden ",
			"popup":"<h1>Haus zum Rüden (Constaffel Society)</h1><h2>Limmatquai,42, 8001</h2><img style='width:100%;' src='images/cha/guildsman/Zum-Ruden-L.JPEG'/><p>Fun Tips: Try a Turicum gin, on tap, in the bar. See the wooden barrel-vaulted ceiling in the Gotischer Saal with a fine view over the Limmat. A male dog ('Rüde' as in 'Haus zum Rüden') with a red collar was the emblem of the Constaffel (noblemen, knights and all those Zurich citizens not members of a craft related guild). Enjoy the powerful images of the red dog in and outside the house.</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37157271017566, 8.543080691803041],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Haue",
			"popup":"<h1>Zunfthaus zur Haue (Greengrocers Guild)</h1><h2>Limmatquai,52, 8001</h2><img style='width:100%;' src='images/cha/guildsman/Zur-Haue-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37171650927014, 8.543240449607884],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Saffran",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37242800682717, 8.544025349610736],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Schmiden",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37042727934282, 8.540333136630045],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Waag",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.370277080935736, 8.541801964615052],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Meisen",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36900748475501, 8.545600493519855],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Weisser Wind",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.36999381426706, 8.539609321605209],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Savoy",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.37270237057181, 8.539815323352475],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Widder",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.37273131037968, 8.543250647803708],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Königsstuhl ",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.37131410996274, 8.541722605971348],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Storchen",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.365997798899, 8.546037303866067],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Sechseläutenplatz",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai,54, 8001</h2><img style='width:100%;' src='images/cha/guildsman/saffran-L.JPEG'/><p>Fun Tips: Savour the views over the Limmat river</p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		],
		"Rower":[
			{
		
					"coordiates":[47.37151131213748, 8.532697150081404],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach1",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36842667701106, 8.537419954221503],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach2",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36586876632113, 8.54013424181896],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach3",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36599105643081, 8.541025698937556],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach4",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.35777188953888, 8.545721573495454],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach41",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.352131690124345, 8.548253578764847],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach42",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				 {
		
					"coordiates":[47.352756349500154, 8.553149317199228],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach5",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34951489774295, 8.556879562979487],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach51",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.349867276907354, 8.560588890465732],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach6",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34514957856003, 8.559059078220912],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach61",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.33818955955939, 8.568512740832203],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach7",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34811991249315, 8.536552062214106],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach8",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36599105643081, 8.541025698937556],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach9",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.366823665854376, 8.541794506890595],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach10",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.368682585605214, 8.54199545081705],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach11",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
	
				"coordiates":[47.3715325105001, 8.532413863400956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Schanzengraben(Men)",
				"popup":"<h1>Zürichhorn</h1><h2>Badweg,10, 8001</h2><img style='width:100%;' src='images/cha/rower/Schanzengraben-L.JPEG'/><p>Fun Tips: Try the after party in the Rimini bar in this Badi</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			  {
	
				"coordiates":[47.368682585605214, 8.54199545081705],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Stadthausquai",
				"popup":"<h1>Stadthausquai (Women)</h1><h2>Stadthausquai,12, 8001</h2><img  style='width:100%;' src='images/cha/rower/Stadthausquai-Women-L.JPEG'/><p>Fun Tips: Try the after party in the Barfuss bar in this Badi</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
		{
	
				"coordiates":[47.3618190419381, 8.536610633106191],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Enge",
				"popup":"<h1>Enge</h1><h2>Mythenquai,9, 8002</h2><img style='width:100%;' src='images/cha/rower/Enge-L.JPEG'/><p>Fun Tips: Go for the view of the alps</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.36198317757748, 8.547126150210916],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Utoquai",
				"popup":"<h1>Utoquai</h1><h2>Utoquai,50, 8008</h2><img style='width:100%;' src='images/cha/rower/Utoquai-L.JPEG'/><p>Fun Tips: Go for the added bar and tapas restaurant </p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			 {
	
				"coordiates":[47.353429934706746, 8.53450210680818],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Mythenquai",
				"popup":"<h1>Mythenquai</h1><h2>Mythenquai,95, 8002</h2><img style='width:100%;' src='images/cha/rower/Mythenquai-L.JPEG'/><p>Fun Tips: Go for the Züriseeüberquerung on July 7 in 2021 (Swimming 1.5 km across the lake from Badi Mythenquai to Badi Tiefenbrunnen)</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35269613664207, 8.557353964614366],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen",
				"popup":"<h1>Tiefenbrunnen</h1><h2>Bellerivestr,200,8002</h2><img style='width:100%;' src='images/cha/rower/Tiefenbrunen-L.JPEG'/><p>Fun Tips: Try the expansive sunbathing areas on the grass and the added table tennis, water slide, and diving board</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.356610519254204, 8.536209450390956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Boathouses",
				"popup":"<h1>Boathouses</h1><h2>Mythenquai,81, 8002</h2><img style='width:100%;' src='images/cha/rower/Boathouses-L.JPEG'/><p>Fun Tips: See the unique city 'succulent' collection on the street side of the boathouses (4400 different varieties of succulents: plants from dry areas that store water to stay alive)</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.33818955955939, 8.568512740832203],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zollikon",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.34117087355733, 8.537431706807455],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Wollishofen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.366208894232535, 8.540591506809635],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Bürkliplatz",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35277311798006, 8.553144533163572],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zürichhorn",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.349867276907354, 8.560588890465732],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.34811991249315, 8.536552062214106],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Wollishofen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			}
		  ],
		

		"Beachgirl":[
			{
		
					"coordiates":[47.37151131213748, 8.532697150081404],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach1",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36842667701106, 8.537419954221503],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach2",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36586876632113, 8.54013424181896],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach3",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36599105643081, 8.541025698937556],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach4",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.35777188953888, 8.545721573495454],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach41",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.352131690124345, 8.548253578764847],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach42",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				 {
		
					"coordiates":[47.352756349500154, 8.553149317199228],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach5",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34951489774295, 8.556879562979487],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach51",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.349867276907354, 8.560588890465732],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach6",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34514957856003, 8.559059078220912],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach61",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.33818955955939, 8.568512740832203],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach7",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34811991249315, 8.536552062214106],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach8",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36599105643081, 8.541025698937556],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach9",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.366823665854376, 8.541794506890595],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach10",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.368682585605214, 8.54199545081705],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach11",
					"popup":"<h1>1FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
	
				"coordiates":[47.3715325105001, 8.532413863400956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Schanzengraben(Men)",
				"popup":"<h1>Zürichhorn</h1><h2>Badweg,10, 8001</h2><img style='width:100%;' src='images/cha/rower/Schanzengraben-L.JPEG'/><p>Fun Tips: Try the after party in the Rimini bar in this Badi</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			  {
	
				"coordiates":[47.368682585605214, 8.54199545081705],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Stadthausquai",
				"popup":"<h1>Stadthausquai (Women)</h1><h2>Stadthausquai,12, 8001</h2><img  style='width:100%;' src='images/cha/rower/Stadthausquai-Women-L.JPEG'/><p>Fun Tips: Try the after party in the Barfuss bar in this Badi</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
		{
	
				"coordiates":[47.3618190419381, 8.536610633106191],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Enge",
				"popup":"<h1>Enge</h1><h2>Mythenquai,9, 8002</h2><img style='width:100%;' src='images/cha/rower/Enge-L.JPEG'/><p>Fun Tips: Go for the view of the alps</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.36198317757748, 8.547126150210916],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Utoquai",
				"popup":"<h1>Utoquai</h1><h2>Utoquai,50, 8008</h2><img style='width:100%;' src='images/cha/rower/Utoquai-L.JPEG'/><p>Fun Tips: Go for the added bar and tapas restaurant </p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			 {
	
				"coordiates":[47.353429934706746, 8.53450210680818],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Mythenquai",
				"popup":"<h1>Mythenquai</h1><h2>Mythenquai,95, 8002</h2><img style='width:100%;' src='images/cha/rower/Mythenquai-L.JPEG'/><p>Fun Tips: Go for the Züriseeüberquerung on July 7 in 2021 (Swimming 1.5 km across the lake from Badi Mythenquai to Badi Tiefenbrunnen)</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35269613664207, 8.557353964614366],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen",
				"popup":"<h1>Tiefenbrunnen</h1><h2>Bellerivestr,200,8002</h2><img style='width:100%;' src='images/cha/rower/Tiefenbrunen-L.JPEG'/><p>Fun Tips: Try the expansive sunbathing areas on the grass and the added table tennis, water slide, and diving board</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.356610519254204, 8.536209450390956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Boathouses",
				"popup":"<h1>Boathouses</h1><h2>Mythenquai,81, 8002</h2><img style='width:100%;' src='images/cha/rower/Boathouses-L.JPEG'/><p>Fun Tips: See the unique city 'succulent' collection on the street side of the boathouses (4400 different varieties of succulents: plants from dry areas that store water to stay alive)</p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.33818955955939, 8.568512740832203],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zollikon",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.34117087355733, 8.537431706807455],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Wollishofen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.366208894232535, 8.540591506809635],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Bürkliplatz",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35277311798006, 8.553144533163572],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zürichhorn",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.349867276907354, 8.560588890465732],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.34811991249315, 8.536552062214106],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Wollishofen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			}
		  ]
	  
	}

var greenIcon = new LeafIcon({iconUrl: 'https://lifeonline.co/map/demo/green.png'});	
		
var url_string = window.location.href; //window.location.href
var url = new URL(url_string);
var character = url.searchParams.get("character");


const cat = character;
const waypointarray = [];
const waypointarray1 = [];
for (x in categorys) {
	

	if(x == cat || cat == 'all'){ 
		
		var lineColor = cat =="all" ? "black" : categorys[x][0].lineColor;
		categorys[x].forEach(function(value,key){

			if(value.is_waypoints == "1" && cat != 'all' ){ // While Run category when not come all

			waypointarray.push(L.latLng(value.coordiates[0],value.coordiates[1]));
			}

			if(value.is_all_waypoints != "0" && cat == 'all' ){ // WHile run category is All

			waypointarray.push(L.latLng(value.coordiates[0],value.coordiates[1]));
			}

			if(value.is_all_waypoints == "2" && cat == 'all' ){ // WHile run category is All

			waypointarray1.push(L.latLng(value.coordiates[0],value.coordiates[1]));
			}
			
		})
	}
	
}

for (x in categorys) {
	console.log(cat);

	if(x == cat || cat == 'all'){ 
		
		categorys[x].forEach(function(value,key){

			if(value.is_waypoints == "0"){

				//console.log(value.coordiates);
					L.marker(value.coordiates, {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="'+value.icon+'"/>'+
              '<div class="my-div-span">'+value.pointernae+'</div>'
    })}).bindPopup(value.popup).addTo(map);

}
		})
	}
   
  }

  var control = L.Routing.control({
	/* Pass in the 1st and last coords in the selected list as waypoints.
	   This Routing sets the initial zoom level of the map.
	   DO not delete this unless you have an alternative */
	waypoints:[waypointarray[0],waypointarray[waypointarray.length-1]], 
	geocoder: L.Control.Geocoder.nominatim(),
	lineOptions: {
      styles: [{color: lineColor, opacity: 0, weight: 0}]
   },
	createMarker: function() { return null; }

}).addTo(map);


  var firstpolyline = new L.Polyline(waypointarray, {
    color: lineColor,
    weight: 3,
    opacity: 1,
    smoothFactor: 1
});
firstpolyline.addTo(map);

/*
console.log('waypointarray2');
console.log(waypointarray1);
console.log('waypointarray1');

  var firstpolyline1 = new L.Polyline(waypointarray1, {
    color: lineColor,
    weight: 4,
    opacity: 1,
    smoothFactor: 1
});
firstpolyline1.addTo(map);*/


/*	L.marker([47.3766937, 8.5403454], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="http://lifeonline.co/map/demo/green.png"/>'+
              '<div class="my-div-span">Pointer</div>'
    })}).bindPopup("I am a green leaf.").addTo(map);



	L.marker([47.3704873, 8.5354656], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="'+green+'"/>'+
              '<div class="my-div-span">'+green+'</div>'
    })}).bindPopup(green).addTo(map);
	/*L.marker([47.3742229, 8.5335609], {icon: orangeIcon}).bindPopup("I am an orange leaf1.").addTo(map);*/
	/*L.marker([47.3753395, 8.5363169], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="http://lifeonline.co/map/demo/green.png"/>'+
              '<div class="my-div-span">Pointer2</div>'
    })}).bindPopup("I am an orange leaf2.").addTo(map);
	L.marker([47.377052, 8.5393448], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="http://lifeonline.co/map/demo/green.png"/>'+
              '<div class="my-div-span">Pointer3</div>'
    })}).bindPopup("I am an orange leaf3.").addTo(map);
    	L.marker([47.3686341, 8.5367769], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="http://lifeonline.co/map/demo/green.png"/>'+
              '<div class="my-div-span">Pointer4</div>'
    })}).bindPopup("<h1>I am an orange leaf3.</h1>").addTo(map);
        	L.marker([47.3721811, 8.5413182], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="http://lifeonline.co/map/demo/green.png"/>'+
              '<div class="my-div-span">Pointer5</div>'
    })}).bindPopup("I am an orange leaf3.").addTo(map);
        	L.marker([47.3637311, 8.5315812], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="http://lifeonline.co/map/demo/green.png"/>'+
              '<div class="my-div-span">FIFA World Football Museum</div>'
    })}).bindPopup("<h1>FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.JPEG'/>").addTo(map);
L.Routing.errorControl(control).addTo(map);
*/


