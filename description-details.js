const peoples = [



{

        "name":"all",

        "imageName":"Tower.png",

        "title":"Hightime Zurich Tour",

        "subtitle":"All-in-one",

        "description":"<p class='fs_14 cl_black'>Hightime offers you the artful essence of Zurich. Where better to enjoy the app than in the centre of town.</p><p class='fs_14 cl_black'>Our city tour takes you past the Zurich landmarks that you can see on top of the tower in the Glockenspiel, past statues of the Zurich lion, the ‘Zürileu’ is the central figure in hightime, and, of course, past the best places to view the Glockenspiel Parade and Parade figures.</p><p class='fs_14 cl_black'>We have added just a little information on what to do on your walk through Zurich in addition to enjoying hightime.To get these tips, zoom-in on the map and press points-of-interest circles.</p><p class='fs_14 cl_black'>For shorter walking routes focused on individual Parade figures, see Special Tours maps</p>"





    },

        {

        "name":"Dandy",

        "imageName":"Dandy.png",

        "title":"Dandy (Salesman)",

        "subtitle":"The Luxury Line",

        "description":"<p class='fs_14 cl_black'>The Dandy is the master of ceremonies in the Glockenspiel and welcomes you to the show. He works and shops in one of the luxury goods stores on the Bahnhofstrasse. That is where to see and snap him. If you walk up the Bahnhofstrasse, you can’t miss the luxury goods stores.</p><p class='fs_14 cl_black'>The clothing brands on the Bahnhofstrasse are mostly global but Swiss brands are strong in accessories: watches, jewellery, and shoes; underwear (leveraging Zurich’s historical role as a centre of the silk industry); and skincare (leveraging Switzerland’s reputation for spas and health resorts). See the points on the map for a few key locations for Swiss brands.</p><p class='fs_14 cl_black'>If you don’t see the Swiss brands on the map, try the Jelmoli department store, it has most.</p>"





    },



     {

        "name":"Trader",

        "imageName":"Trader.png",

        "title":"Trader",

        "subtitle":"The Money Line",

        "description":"<p class='fs_14 cl_black'>The hightime trader works in a big Zurich bank. Banks and insurance companies are still the biggest employers in Zurich’s city centre. The banks are on the Bahnhofstrasse, more towards the lake end than the big stores. The insurance companies are on the West side of the lake. The money line on the map connects them all.</p><p class='fs_14 cl_black'>Credit Suisse is the traditional doyen of the banks and commands pride of place at the Paradeplatz, Zurich’s commercial centre. The inner courtyard of the Credit Suisse building, next to the main entrance, is a good place to snap the Trader.</p>",





    },

     {

        "name":"Cook",

        "imageName":"Cook.png",

        "title":"Cook",

        "subtitle":"Classic Restaurants",

        "description":"<p class='fs_14 cl_black'>Zurich people relish going out to eat and there are good restaurants open everywhere (except on Sundays and during lockdown).</p><p class='fs_14 cl_black'>The essence of modern Zurich cuisine may be Italian but you will usually find the hightime Cook in one of the real old Swiss restaurants on the map.  Many have interesting interiors (as well as exteriors), so you may want to snap the Cook indoors.</p><p class='fs_14 cl_black'>The Guildhouses all have traditional Swiss restaurants, so you may find the hightime Cook there too (see Sechseläuten map). But not during Sechseläuten because the Cook is a Guildsman and will take the day off to celebrate.</p>",





    },

     {

        "name":"Roadworker",

        "imageName":"Roadwork.png",

        "title":"Roadworker",

        "subtitle":"",

        "description":"<p class='fs_14 cl_black'>Zurich’s roads are always in perfect condition. But to keep them that way requires a lot of repairs. Extensive roadworks are an obstacle that Zurich residents, and the Hightime characters, have to get around.</p><p class='fs_14 cl_black'>Remarkably, there are few major roadworks in the city centre at present, so you’ll have to try hard to find the Hightime Roadworker.  You may see him on the Talstrasse, at the Postbrücke near the Hauptbahnhof (main station). Or, most conveniently for the Hightime Zurich Tour, on the Utoquai.</p>",





    },

     {

        "name":"Dog",

        "imageName":"Dog.png",

        "title":"Dog",

        "subtitle":"Green routes",

        "description":"<p class='fs_14 cl_black'>You find dogs everywhere in Zurich but the city has no Central Park for them to run about and exercise.</p><p class='fs_14 cl_black'>For a short walk for the hightime Dog, try the Seefeldquai between the Bellevue and the Zürihorn</p><p class='fs_14 cl_black'>For a longer walk, travel to the larger green areas outside Zurich on the hills on both sides of the lake.</p><p class='fs_14 cl_black'>For example, from the Bellevue, take tram number 5 to the Zoo. From the tram stop, turn left along the Orelli Strasse, or right along the Dreiwiesenstrasse and head for the woods.</p><p class='fs_14 cl_black'>(Dogs need a ticket on Zurich trams and are not allowed in the Zoo! But the hightime Dog rides for free and can do what he likes.)  </p>",





    },

     {

        "name":"Raver",

        "imageName":"Raver.png",

        "title":"Raver",

        "subtitle":"Street Parade Route",

        "description":"<p class='fs_14 cl_black'>The Dandy is a techno music fan. So you will find him as a Raver at the Street Parade, Europe’s biggest techno street party</p><p class='fs_14 cl_black'>The Street Parade route takes you round the city end of the lake. The real parade, which takes place once a year in August, goes from East to West but take the other direction to stay in the sun.</p><p class='fs_14 cl_black'>Just like the pioneering Ravers at the parade, the statues that border the route exhibit love, Aphrodite and Ganymede, as well as youthful courage, David.</p>",





    },

     {

        "name":"Guildsman",

        "imageName":"Guildsman.png",

        "title":"Guildsman",

        "subtitle":"Sechseläuten Route",

        "description":"<p class='fs_14 cl_black'>Zurich’s Sechseläuten celebrates the end of winter. The parade route goes round the city centre and finishes in front of the Opera House where, at the end of the parade, the Guildsmen ride around the Böögg, a sort of exploding snowman on top of a pyre. If the Böögg explodes quickly after the pyre is set alight, then it will be a good summer. </p><p class='fs_14 cl_black'>We chose the Sechseläuten march as the background music to hightime. It is Zurich’s signature tune.<p><p class='fs_14 cl_black'>After the parade, all the guilds enjoy a good dinner in their guild houses. The most traditional of these are on or close to the parade route.</p><p class='fs_14 cl_black'>Catch the hightime Guildsman in front of a guild house, near the Opera, or on his way there on an e-scooter.</p>",





    },

     {

        "name":"Beachgirl",

        "imageName":"beachgirl.png",

        "title":"Beachgirl",

        "subtitle":"Water Line",

        "description":"<p class='fs_14 cl_black'>They say, if you live in Zurich, stay in the city in summer and take your vacation some other time. Summer is great to go swimming. There are plenty of lake or riverside “Badi’s” (“Badeanstalten” or “bathing establishments”). Several are near the centre of town so you can go before or after work or at lunchtime.</p><p class='fs_14 cl_black'>The Trader is a Badi Girl and loves to go for a swim. She quite likes the after swim bars at the city centre Badis as well. Snap her as she arrives at the Badi on her e-scooter</p><p class='fs_14 cl_black'>Stand-Up-Paddling and pedal boating are popular too in Zurich. You can SUP in the Lakeside Badis and, there are SUP and pedal boat offers along the Utoquai.</p><p class='fs_14 cl_black'>You can also see Zurich’s bathers and rowers on a mini lake cruise on a ZSG ferry boat</p>",





    },

     {

        "name":"Rower",

        "imageName":"rover.png",

        "title":"Rower",

        "subtitle":"Boathouses",

        "description":"<p class='fs_14 cl_black'>The Roadworker has strong arms and a powerful body, so no surprise that he is a keen competitive Rower.</p><p class='fs_14 cl_black'>Lake Zurich is 40 km long and an ideal rowing basin but only its northern tip is part of the city; the rowing clubs all cohabit a small patch of land on the south western border of Zurich.</p><p class='fs_14 cl_black'>The Grasshopper Club Rowers have used what little space is available to build a giant palace of a boat house. On shore,  that is the place to snap the rower with his imposing pair of oars. The view of the lake and mountains is spectacular.</p><p class='fs_14 cl_black'>You can also see Zurich rowers and bathers from a mini lake cruise on a ZSG ferry boat.</p>",





    }



    ]



    console.log(peoples);

   







